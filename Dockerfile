FROM centos:8

MAINTAINER Zulfadzly <zrazak@silversky.comm>

ADD . /customerFeedback

RUN yum -y update && \
    yum -y install \
	telnet \
	python36 \
    wget && \
    curl https://bootstrap.pypa.io/get-pip.py | python3.6 && \
    yes | pip3 install -r /customerFeedback/requirements.txt && \
    yum clean all && \
    rm -rf /var/cache/yum

WORKDIR /customerFeedback

EXPOSE 5001

CMD ["python3", "-u", "manage.py run", "-p 5001"]