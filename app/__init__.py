from flask_restx import Api
from flask import Blueprint

from .main.controller.survey_controller import api as survey_ns

blueprint = Blueprint('api', __name__)

authorizations = {
    'apiKey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'X-API-KEY'
    }
}

api = Api(blueprint,
          title='Feedback',
          version='1.0',
          description='Customer feedback API',
          authorizations=authorizations
          )

api.add_namespace(survey_ns, path='/api/customerFeedback')