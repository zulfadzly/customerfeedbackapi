from flask import request
from flask_restx import Resource,  Namespace, fields
from functools import wraps
from ..config import token

from ..util.dto import SurveyDto
from ..service.survey_service import save_new_survey, get_all_survey, get_survey, delete_survey
from ..service.question_service import save_new_question, get_all_question, delete_question
from ..service.answer_service import save_new_answer, get_all_answer, delete_answer

api = SurveyDto.api
_survey = SurveyDto.survey
_createSurvey = SurveyDto.create_survey
_question = SurveyDto.question
_answer = SurveyDto.answer
_createQuestion = SurveyDto.create_question


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwagrs):

        request_token = None

        if 'X-API-KEY' in request.headers:
            request_token = request.headers['X-API-KEY']

        if not token:
            return {'message': 'Token is missing.'}, 401

        if request_token != token:
            return {'message': 'wrong token passed.'}, 401

        return f(*args, ** kwagrs)

    return decorated


@api.route('/survey')
class SurveyList(Resource):
    @api.doc(security='apiKey')
    @token_required
    @api.doc('list_of_survey')
    @api.marshal_list_with(_survey, envelope='data')
    def get(self):
        """ List all survey """
        return get_all_survey()

    @api.response(201, 'survey successfully created.')
    @api.doc(security='apiKey')
    @token_required
    @api.doc('create_new_survey')
    @api.expect(_createSurvey, validate=True)
    def post(self):
        """ Creates a new survey """
        data = request.json
        return save_new_survey(data=data)


@api.route('/survey/<int:id>')
@api.param('id', 'The survey identifier')
class GetSurvey(Resource):
    @api.doc(security='apiKey')
    @token_required
    @api.doc('delete_survey')
    def delete(self, id):
        """ Delete survey """
        return delete_survey(id)

    @api.doc(security='apiKey')
    @token_required
    @api.doc('get_survey')
    @api.marshal_list_with(_survey)
    def get(self, id):
        """ Get Survey """
        survey = get_survey(id)
        if survey:
            return survey
        else:
            api.abort(
                code=400,
                message="Sorry, Survey doesn't exist."
            )


@api.route('/question')
class QuestionList(Resource):
    @api.doc('list_of_survey_question')
    @api.marshal_list_with(_question, envelope='data')
    def get(self):
        """List all question"""
        return get_all_question()

    @api.response(201, 'survey successfully created.')
    @api.doc('create_new_survey_question')
    @api.expect(_createQuestion, validate=True)
    def post(self):
        """Creates a new survey question """
        data = request.json
        return save_new_question(data=data)


@api.route('/question/<int:id>')
@api.param('id', 'The question identifier')
class DeleteQuestion(Resource):
    @api.doc('delete_survey_question')
    def delete(self, id):
        """ Delete survey question """
        return delete_question(id)


@api.route('/answer')
class AnswerList(Resource):
    @api.doc(security='apiKey')
    @token_required
    @api.doc('list_of_survey_answer')
    @api.marshal_list_with(_answer, envelope='data')
    def get(self):
        """ List all survey answer """
        return get_all_answer()

    @api.response(201, 'answer successfully created.')
    @api.doc(security='apiKey')
    @token_required
    @api.doc('create_new_survey_answer')
    @api.expect(_answer, validate=True)
    def post(self):
        """Answered a survey question"""
        data = request.json
        return save_new_answer(data=data)


@api.route('/answerByQuestion/<int:id>')
@api.param('id', 'Question identifier')
class GetAnswer(Resource):
    @api.doc(security='apiKey')
    @token_required
    @api.doc('get_survey_answer_by_question')
    def get(self, id):
        """ Get answer by question """
        return get_all_answer_by_question(id)


@api.route('/answer/<int:id>')
@api.param('id', 'The answer identifier')
class DeleteAnswer(Resource):
    @api.doc(security='apiKey')
    @token_required
    @api.doc('delete_survey_answer')
    def delete(self, id):
        """ Delete an answer from survey question """
        return delete_answer(id)