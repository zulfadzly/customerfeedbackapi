from .. import db
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Answer(db.Model, Base):

    """ Survey Question Answer Model for storing survey question answer """
    __tablename__ = "survey_answer"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    question_id = db.Column(db.Integer, db.ForeignKey('survey_question.id'))
    user_id = db.Column(db.Integer, nullable=False)
    answer = db.Column(db.String(255), nullable=False)
    show_status = db.Column(db.String(20), nullable=False, default="active")
    create_date = db.Column(db.DateTime, nullable=False)
    update_date = db.Column(db.DateTime, nullable=False)

    def __init__(self, answer, question_id, user_id, create_date, update_date):
        self.answer = answer
        self.question_id = question_id
        self.user_id = user_id
        self.create_date = create_date
        self.update_date = update_date