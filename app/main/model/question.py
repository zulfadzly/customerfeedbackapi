from .. import db
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Question(db.Model, Base):

    """ Survey Question Model for storing survey question """
    __tablename__ = "survey_question"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    survey_id = db.Column(db.Integer, db.ForeignKey('survey.id'))
    question_text = db.Column(db.String(255), nullable=False)
    question_description = db.Column(db.String(255), nullable=True)
    question_type = db.Column(db.Integer, default=1)
    order = db.Column(db.Integer, default=-100)
    show_status = db.Column(db.String(20), nullable=False, default="active")
    create_date = db.Column(db.DateTime, nullable=False)
    update_date = db.Column(db.DateTime, nullable=False)
    answers = db.relationship("Answer", backref="question", lazy='dynamic', cascade="all, delete-orphan")
    questionOptions = db.relationship("QuestionOption", backref="questionOption", lazy='dynamic',
                                      cascade="all, delete-orphan")

    def __init__(self, question_text, question_description, question_type, survey_id, create_date, update_date):
        self.question_text = question_text
        self.question_description = question_description
        self.question_type = question_type
        self.survey_id = survey_id
        self.create_date = create_date
        self.update_date = update_date