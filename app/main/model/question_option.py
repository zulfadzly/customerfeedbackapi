from .. import db
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class QuestionOption(db.Model, Base):

    """ Survey Question Option Model for storing survey question option """
    __tablename__ = "question_option"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    question_id = db.Column(db.Integer, db.ForeignKey('survey_question.id'))
    option_text = db.Column(db.String(255), nullable=False)
    option_description = db.Column(db.String(255), nullable=True)
    order = db.Column(db.Integer, default=-100)
    show_status = db.Column(db.String(20), nullable=False, default="active")
    create_date = db.Column(db.DateTime, nullable=False)
    update_date = db.Column(db.DateTime, nullable=False)

    def __init__(self, option_text, option_description, question_id, create_date, update_date):
        self.option_text = option_text
        self.option_description = option_description
        self.question_id = question_id
        self.create_date = create_date
        self.update_date = update_date