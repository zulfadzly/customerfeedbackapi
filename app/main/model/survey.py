from .. import db
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Survey(db.Model, Base):

    """ Survey Model for storing survey related details """
    __tablename__ = "survey"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    platform_id = db.Column(db.Integer)
    customer_id = db.Column(db.Integer, nullable=False, default=0)
    service = db.Column(db.String(255), nullable=False)
    section = db.Column(db.String(255), nullable=False)
    title = db.Column(db.String(255), nullable=False)
    description = db.Column(db.String(255), nullable=False)
    internal = db.Column(db.String(4), nullable=False, default="yes")
    show_status = db.Column(db.String(20), nullable=False, default="active")
    create_date = db.Column(db.DateTime, nullable=False)
    update_date = db.Column(db.DateTime, nullable=False)
    questions = db.relationship("Question", backref="survey", lazy='dynamic', cascade="all, delete-orphan")

    def __init__(self, platform_id, customer_id, service, section, title, description, create_date, update_date):
        self.platform_id = platform_id
        self.customer_id = customer_id
        self.service = service
        self.section = section
        self.title = title
        self.description = description
        self.create_date = create_date
        self.update_date = update_date