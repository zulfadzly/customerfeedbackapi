import uuid
import datetime

from app.main import db
from app.main.model.answer import Answer
from sqlalchemy.exc import SQLAlchemyError
import logging


def save_new_answer(data):

    new_answer = Answer(
        answer=data['answer'],
        question_id=data['question_id'],
        user_id=data['user_id'],
        create_date=datetime.datetime.utcnow(),
        update_date=datetime.datetime.utcnow(),
    )

    if save_changes(new_answer):
        response_object = {
            'status': 'success',
            'message': 'Survey answer successfully created.'
        }
        return response_object, 201
    else:
        response_object = {
            'status': 'fail',
            'message': 'Failed to create survey answer',
        }
        return response_object, 409


def delete_answer(id):

    if process_delete_answer(id):
        response_object = {
            'status': 'success',
            'message': 'Answer deleted.'
        }
        return response_object, 201
    else:
        response_object = {
            'status': 'fail',
            'message': 'Failed to delete answer',
        }
        return response_object, 409


def get_all_answer():
    return Answer.query.all()


def get_all_answer_by_question(question_id):
    return Answer.query.all()


def save_changes(new_answer):
    try:
        answer = new_answer
        db.session.add(answer)
        db.session.commit()
        return True
    except Exception as e:
        db.session.rollback()
        db.session.flush()
        return False


def process_delete_answer(answer_id):
    try:
        answer = Answer.query.filter_by(id=answer_id).one()
        db.session.delete(answer)
        db.session.commit()
        return True
    except Exception as e:
        print(e)
        db.session.rollback()
        db.session.flush()
        return False