import uuid
import datetime

from app.main import db
from app.main.model.question import Question
from app.main.model.question_option import QuestionOption
from app.main.model.answer import Answer
from sqlalchemy.exc import SQLAlchemyError
import logging


def save_new_question(data):

    new_question = Question(
        question_text=data['question_text'],
        question_description=data['question_description'],
        question_type=data['question_type'],
        survey_id=data['survey_id'],
        create_date=datetime.datetime.utcnow(),
        update_date=datetime.datetime.utcnow(),
    )

    if save_changes(new_question, data['options']):
        response_object = {
            'status': 'success',
            'message': 'Survey question successfully created.'
        }
        return response_object, 201
    else:
        response_object = {
            'status': 'fail',
            'message': 'Failed to create survey question',
        }
        return response_object, 409


def delete_question(id):

    if process_delete_question(id):
        response_object = {
            'status': 'success',
            'message': 'Question deleted.'
        }
        return response_object, 201
    else:
        response_object = {
            'status': 'fail',
            'message': 'Failed to delete question',
        }
        return response_object, 409


def get_all_question():
    return Question.query.all()


def save_changes(new_question, options):
    try:
        question = new_question
        db.session.add(question)
        db.session.flush()
        if options:
            for option in options:
                new_question_option = QuestionOption(
                    option['option_text'],
                    option['option_description'],
                    new_question.id,
                    datetime.datetime.utcnow(),
                    datetime.datetime.utcnow(),
                )
                db.session.add(new_question_option)
                db.session.flush()
        db.session.commit()
        return True
    except Exception as e:
        print(e)
        db.session.rollback()
        db.session.flush()
        return False


def process_delete_question(question_id):
    try:
        question = Question.query.filter_by(id=question_id).one()
        db.session.delete(question)
        db.session.commit()
        return True
    except SQLAlchemyError as e:
        print(e)
        db.session.rollback()
        db.session.flush()
        return False