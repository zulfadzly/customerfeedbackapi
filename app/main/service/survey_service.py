import uuid
import datetime

from app.main import db
from app.main.model.survey import Survey
from app.main.model.question import Question
from app.main.model.question_option import QuestionOption
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm.exc import NoResultFound
import logging
import sys
from ..config import platform, deleted_status, active_status


def save_new_survey(data):

    new_survey = Survey(
        platform_id=platform,
        customer_id=data['customer_id'],
        service=data['service'],
        section=data['section'],
        title=data['title'],
        description=data['description'],
        create_date=datetime.datetime.utcnow(),
        update_date=datetime.datetime.utcnow()
    )

    if save_changes(new_survey, data['questions']):
        response_object = {
            'status': 'success',
            'message': 'Survey successfully created.'
        }
        return response_object, 201
    else:
        response_object = {
            'status': 'fail',
            'message': 'Failed to create survey',
        }
        return response_object, 409


def delete_survey(id):

    if process_delete_survey(id):
        response_object = {
            'status': 'success',
            'message': 'Survey deleted'
        }
        return response_object, 201
    else:
        response_object = {
            'status': 'fail',
            'message': 'Failed to delete survey',
        }
        return response_object, 409


def get_all_survey():
    return Survey.query.all()


def get_survey(survey_id):
    survey = Survey.query.filter_by(id=survey_id).first()
    if Survey is None:
        return False
    else:
        return survey


def save_changes(new_survey, questions):
    try:
        newSurvey = new_survey
        db.session.add(newSurvey)
        db.session.flush()
        if questions:
            for question in questions:
                new_question = Question(
                    question['question_text'],
                    question['question_description'],
                    question['question_type'],
                    newSurvey.id,
                    datetime.datetime.utcnow(),
                    datetime.datetime.utcnow(),
                )
                db.session.add(new_question)
                db.session.flush()
                if question['options']:
                    for option in question['options']:
                        new_question_option = QuestionOption(
                            option['option_text'],
                            option['option_description'],
                            new_question.id,
                            datetime.datetime.utcnow(),
                            datetime.datetime.utcnow(),
                        )
                        db.session.add(new_question_option)
                        db.session.flush()
        db.session.commit()
        return True
    except SQLAlchemyError as e:
        print(e)
        db.session.rollback()
        db.session.flush()
        return False


def process_delete_survey(survey_id):
    try:
        db.session.query(Survey).filter_by(id=survey_id).delete()
        db.session.commit()
        return True
    except SQLAlchemyError as e:
        print(e)
        db.session.rollback()
        db.session.flush()
        return False