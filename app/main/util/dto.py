from flask_restx import Namespace, fields


class SurveyDto:

    api = Namespace('feedback', description='feedback related operations')

    answerData = api.model('answerData', {
        'id': fields.Integer,
        'user_id': fields.Integer,
        'answer': fields.String,
        'show_status': fields.String,
    })

    createSurveyQuestionOptionData = api.model('createSurveyQuestionOptionData', {
        'id': fields.Integer,
        'option_text': fields.String,
        'option_description': fields.String,
        'show_status': fields.String,
    })

    createQuestionOptionData = api.model('createQuestionOptionData', {
        'id': fields.Integer,
        'option_text': fields.String,
        'option_description': fields.String,
    })

    questionOptionData = api.model('createQuestionOptionData', {
        'id': fields.Integer,
        'option_text': fields.String,
        'option_description': fields.String,
        'show_status': fields.String,
    })

    questionData = api.model('questionData', {
        'id': fields.Integer,
        'question_text': fields.String,
        "question_description": fields.String,
        'question_type': fields.Integer,
        'show_status': fields.String,
        'answers': fields.List(
            fields.Nested(
                answerData
            ),
        )
    })

    createSurveyQuestionData = api.model('createSurveyQuestionData', {
        'id': fields.Integer,
        'question_text': fields.String,
        'question_description': fields.String,
        'question_type': fields.Integer,
        'show_status': fields.String,
        'options': fields.List(
            fields.Nested(
                createSurveyQuestionOptionData
            ),
        )
    })

    survey = api.model('survey', {
        'id': fields.Integer(description='survey id'),
        'platform_id': fields.Integer(required=True, description='platform id'),
        'customer_id': fields.Integer(required=True, description='identifier for survey created by '
                                                                 'customer of the platform'),
        'service': fields.String(required=True, description='service name of the survey'),
        'section': fields.String(required=True, description='section name of the survey'),
        'title': fields.String(required=True, description='survey title'),
        'description': fields.String(required=True, description='survey description'),
        'internal': fields.String(required=True, description='survey created by platform'),
        'show_status': fields.String(required=True, description='survey status'),
        'questions': fields.List(
            fields.Nested(
                questionData
            ),
        )

    })

    create_survey = api.model('create_survey', {
        'id': fields.Integer(description='survey id'),
        'customer_id': fields.Integer(required=True, description='identifier for survey created by '
                                                                 'customer of the platform'),
        'service': fields.String(required=True, description='service name of the survey'),
        'section': fields.String(required=True, description='section name of the survey'),
        'title': fields.String(required=True, description='survey title'),
        'description': fields.String(required=True, description='survey description'),
        'questions': fields.List(
            fields.Nested(
                createSurveyQuestionData
            ),
        )

    })

    question = api.model('question', {
        'id': fields.Integer(description='question id'),
        'survey_id': fields.Integer(required=True, description='survey id'),
        'question_text': fields.String(required=True, description='question'),
        'question_description': fields.String(description='question'),
        'question_type': fields.Integer(description='question type'),
        'show_status': fields.String(description='question status'),
        'questionOptions': fields.List(
            fields.Nested(
                questionOptionData
            ),
        )
    })

    create_question = api.model('create_question', {
        'id': fields.Integer(description='question id'),
        'survey_id': fields.Integer(required=True, description='survey id'),
        'question_text': fields.String(required=True, description='question'),
        'question_description': fields.String(description='question'),
        'question_type': fields.Integer(description='question type'),
        'options': fields.List(
            fields.Nested(
                createQuestionOptionData
            ),
        )
    })

    answer = api.model('answer', {
        'id': fields.Integer(description='answer id'),
        'question_id': fields.Integer(required=True, description='question id'),
        'user_id': fields.Integer(required=True, description='user id'),
        'answer': fields.String(required=True, description='answer'),
        'show_status': fields.String(description='answer status'),
    })